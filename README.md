# Linux Fuckery

fork bomb:

```
bomb() { bomb|bomb & }; bomb
```

cause kernel panic:

```
echo 1 > /proc/sys/kernel/sysrq
echo c > /proc/sysrq-trigger
```

Put process in uninterruptable sleep:

```
mkdir /sys/fs/cgroup/freezer
mount -t cgroup -ofreezer freezer /sys/fs/cgroup/freezer
mkdir /sys/fs/cgroup/freezer/frozen
echo FROZEN > /sys/fs/cgroup/freezer/frozen/freezer.state
echo $$ > /sys/fs/cgroup/freezer/frozen/tasks

# to unblock:
echo THAWED > /sys/fs/cgroup/freezer/frozen/freezer.state
```

allocate a lot disk space

```
fallocate -l 8.8G large.img
```

Allocate a lot of memory

```
gcc mem.c -o mem.o
./mem.o
```

destroy chmod:

```
chmod -x /bin/chmod

# restore permissions on chmod
setfacl -m u::rx chmod
```