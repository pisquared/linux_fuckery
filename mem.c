#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

long int INTS = 100000000;

int main() {
  printf("%d\n", getpid());
  int* array = malloc(INTS*sizeof(int));

  printf("allocating %d B \n", INTS*sizeof(int));
  for (int i=0; i<INTS; i++) {
    array[i] = i;
  }
  printf("filled %d B \n", INTS*sizeof(int));

  getchar();
  return 0;
}